import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Home from './pages/home';
import Jsflix from './pages/jsflix';
import ReactMirror from './pages/react-mirror'; 


export default class App extends Component{
  render(){
    return (
      <Router>
        <Route path="/" exact component={Home}/>
        <Route path="/react-mirror" component={ReactMirror}/>
        <Route path="/jsflix" component={Jsflix}/>
      </Router>      
    );
  }  
}




