
import React from 'react';
import './App.css';
//import Header from '../components/header';
import ListaEpisodios from '../models/listaEpisodios';
import EpisodioUi from '../models/episodioUi';
import MensagemFlash from '../components/MensagemFlash';
import MeuInputNumero from '../models/MeuInputNumero';

class ReactMirror extends React.Component {
  //
  constructor( props ) {
    super( props )
    this.listaEpisodios = new ListaEpisodios();
    //this.Mensagem = new Mensagem();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      deveExibirMensagem: false,
      deveExibirErro: false
      //cor: "verde",
    }
  }

  sortear() {
    const episodio = this.listaEpisodios.episodiosAleatorios
    //this.sortear = this.sortear.bind ( this)
    this.setState({
      episodio
    })
  }

  assistido() {
    const { episodio } = this.state
    //this.listaEpisodios.marcarComoAssistido( episodio )
    episodio.marcarParaAssistido()
    this.setState({
      episodio
    })
  }

  registrarNota({ nota, erro }) {
    this.setState({
      deveExibirErro: erro
    })
    if ( erro ){
      return;
    }

    let cor, mensagem;
    const { episodio } = this.state
    if( episodio.validarNota( nota )) {
      episodio.avaliar( nota )    
      cor = 'verde';
      mensagem = 'Registramos sua nota!'
    }else{
      //episodio.avaliar(nota)
      cor = 'vermelho'
      mensagem = 'Informar nota válida entre 1 e 5'
    }

     this.exibirMensagem({ cor, mensagem });
  }  

    exibirMensagem = ({ cor, mensagem}) => {
      this.setState ({
        cor,
        mensagem,
        exibirMensagem: true
      })
    }

    /*setTimeout(() => {
      this.setState({
        deveExibirMensagem: false
      })
    }, 5000);
    //this.Mensagem.registrarNota(evento);

  }*/

  /*
  geraCampoDeNota() {
    return (
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Avalie este episódio</span>
              <input type="number" placeholder="1 a 5" min="1" max="5" onBlur={this.registrarNota.bind(this)} />
            </div>
          )
        }
      </div>
    )
  }*/

  atualizarMensagem = devoExibir => {
    this.setState({
      exibirMensagem: devoExibir 
    })
  }

  render() {
    const { episodio, exibirMensagem, cor, mensagem, deveExibirErro } = this.state;

    return (

      <div className="App">

        {/* <Header />  */} 
        {/*  <Mensagem /> */}
        {/*{ deveExibirMensagem ? (<span>Registramos sua nota!</span>) : '' */}

        <MensagemFlash atualizarMensagem={ this.atualizarMensagem }
                       deveExibirMensagem={ exibirMensagem }
                       mensagem={ mensagem }
                       cor= { cor }
                       segundos={ 5 } /> 
        <header className='App-header'>
          {/* <span className={`flash ${cor} ${ deveExibirMensagem ? '' : 'invisivel'}`}>Registramos sua nota!</span> */}
        <EpisodioUi episodio={ episodio } />
        <div className="botoes">
            <button className="btn verde" onClick={this.sortear.bind(this)}>Próximo</button>
            <button className="btn azul" onClick={this.assistido.bind(this)}>Já Assisti</button>
        </div>
          {/* {this.geraCampoDeNota()} */}
        <MeuInputNumero placeholder="1 a 5"
                        mensagemCampo="Qual sua nota para este episódio?"
                        visivel={ episodio.assistido || false }
                        obrigatorio={ true }
                        atualizarValor={ this.registrarNota.bind( this )}  
                        deveExibirErro={ deveExibirErro }
        />                
        </header>
      </div>
    );
  }
}

export default ReactMirror;
