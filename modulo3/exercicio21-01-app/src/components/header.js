import React from 'react';
import { Link } from 'react-router-dom';
import '../App.css';

export default function Header(){
    return(
        <section>
             <header class="main-header">
                <nav class="container">
                    <ul class="App">
                        <li> <Link to="/">Home</Link> </li>
                        <li> <Link to="/react-mirror">React Mirror</Link> </li>
                        <li> <Link to="/jsflix">JsFlix</Link> </li>    
                    </ul>   
                </nav>
            </header>   
        </section>
    )
}

