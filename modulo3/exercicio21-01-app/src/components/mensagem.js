import React from 'react';



export default class Mensagem extends React.Component{
    constructor( props ){
        super( props )   
        this.state = {       
          deveExibirMensagem:false,
          cor: "verde", 
        }
      } 
    
      registrarNota( evento ){
        const { episodio } = this.state
        if (evento.target.value>5 || evento.target.value<1){
          this.setState({
            cor: "vermelho",
            deveExibirMensagem: true
          })          
        } else {
          episodio.avaliar( evento.target.value)
          this.setState({
            cor: "verde",
            episodio,
            deveExibirMensagem: true
          })    
        }
        
        setTimeout(() => {
          this.setState({
            deveExibirMensagem: false
          })
        }, 5000);
    }     

    render(){
        const { cor, deveExibirMensagem } = this.state;
        return(
            <div>
                <span className={`flash ${cor} ${ deveExibirMensagem ? '' : 'invisivel'}`}>Registramos sua nota!</span>     
            </div>
        )
    }  
   
}

