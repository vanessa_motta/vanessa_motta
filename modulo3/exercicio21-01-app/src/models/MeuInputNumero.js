import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class MeuInputNumero extends Component {

    perderFoco = evt =>{
        const { obrigatorio, atualizarValor } = this.props
        const valor = evt.target.value
        const erro = obrigatorio && !valor
        atualizarValor({ nota, erro })
    }

    render(){
        const { placeholder, visivel, mensagemCampo, deveExibirErro } = this.props
        return visivel ? (
            <React.Fragment>
                {
                    mensagemCampo && <span>{ mensagemCampo }</span>
                }
                {
                    <input type="number" className={ deveExibirErro ? 'erro' : ''} onBlur= {this.perderFoco} placeholder={ placeholder }/>
                }
                {
                    deveExibirErro && <span className="mensagem-erro">* Obrigatório</span>
                }
            </React.Fragment>    
        ) : null
    }
}

MeuInputNumero.prototype = {
    visivel: PropTypes.bool.isRequired,
    deveExibirErro: PropTypes.bool.isRequired,
    placeholder: PropTypes.bool.string,
    mensagemCampo: PropTypes.bool.string,  
    obrigatorio: PropTypes.bool
}

MeuInputNumero.defaultProps = {
    
}