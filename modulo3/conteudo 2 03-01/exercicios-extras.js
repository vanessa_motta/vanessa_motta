/*Escreva uma função imprimirGBP que recebe um número flutuante (ex: 4.651) 
e retorne como saída uma string no seguinte formato (seguindo o exemplo): “£ 4.66”

Outros exemplos:

imprimirGBP(0) // “£ 0.00”
imprimirGBP(3498.99) // “£ 3,498.99”
imprimirGBP(-3498.99) // “-£ 3,498.99”
imprimirGBP(2313477.0135) // “£ 2,313,477.02 */

var numero = 4.66

function imprimirGBP (numero){
    var valorArredondado = Math.abs(numero.toFixed(2));
    return console.log(`${numero > 0 ? '' : '-' }£ ${valorArredondado}`);
}

imprimirGBP(-4.66,25)