/*Exercício 01

Crie uma função "calcularCirculo" que receba um objeto com os seguintes parâmetros:

{
    		raio, // raio da circunferência
    		tipoCalculo // se for "A" cálcula a área, se for "C" calcula a circunferência
}

const circulo1 = {
	raio: 4,
	tipoCalculo: "A"  
};

const circulo2 ={
	raio:8,
	tipoCalculo:"C"
};

function calcularCirculo (circulo){
	let resultado;
	if(circulo.tipoCalculo == "A"){
		resultado = Math.pow(3.14 * circulo.raio, 2); 		
	} else if (circulo.tipoCalculo == "C"){
		resultado = 2 * 3.14 * circulo.raio;		
	}
	return resultado;
}
*/

/* 

function calcularCirculo({raio, tipoCalculo:tipo}){
	return tipo == "A" ? Math.PI * Math.pow(raio,2) : 2 * Math.PI * raio

}

let circulo ={
	raio: 3,
	tipoCalculo:"A"
}

console.log(calcularCirculo(circulo));

*/

//console.log(calcularCirculo(circulo1))
//console.log(calcularCirculo(circulo2))

/*
Exercício 02

Crie uma função naoBissexto que recebe um ano (número) e verifica se ele não é bissexto. Exemplo:

	naoBissexto(2016) // false
	naoBissexto(2017) // true

Os anos divisíveis por 4 são bissextos, porém cada 400 anos devem se eliminar 3 bissextos.
Por isso, não são bissextos os que se dividem por 100, menos os que se dividem por 400, 
que sim são bissextos.
É múltiplo de 400. Exemplos: 1200, 1600, 2000, 2400, 2800...
É múltiplo de 4 mas não é de 100. Exemplos: 1996, 2000, 2004, 2008, 2012, 2016...



function naoBissexto (ano) {
	if (ano%400 === 0 ){
		return true;
	} else if(ano%4 === 0 && ano%100 !== 0){
		return true;
	} else {
		return false;
	}
}
*/

//console.log(naoBissexto(2010));

/*

function naoBissexto(ano){
	return (ano %400 === 0) || (ano %4 === 0 && ano %100 !== 0)
}
console.log(naoBissexto(2015))

let bissexto = ano => (ano %400 === 0) || (ano %4 === 0 && ano %100 !== 0);
console.log(bissexto(2016));

*/


/*
Exercício 03

Crie uma função somarPares que recebe um array de números (não precisa fazer validação)
e soma todos números nas posições pares do array, exemplo:

somarPares( [ 1, 56, 4.34, 6, -2 ] ) // 3.34


const arrayexemplo = [1, 56, 4.34, 6, -2];

function somaPares(array){
	let soma = 0;
	for (let i = 0; i<array.length;i++){
		if (i%2 == 0){
			soma += array[i];
		}
	} return soma;
}

console.log(somaPares(arrayexemplo))
*/

/*

function somarPares(numeros){
	let ressultado = 0;
	for (let i = 0; i<numeros.length; i++){
		if(i % 2 == 0){
			resultado += numeros[i];
		}
	}
	return resultado;
}

*/

/*
Exercício 04 - Soma diferentona

Escreva uma função adicionar que permite somar dois números através de duas chamadas diferentes (não necessariamente pra mesma função). Pirou? Ex:

adicionar(3)(4) // 7
adicionar(5642)(8749) // 14391
*/

/*function adicionar(op1){
	return function(op2){
		return op1 + po2;
	}
}

let adicionar = op1 => op2 => op1 + op2;

console.log(adicionar((3)(4));
console.log(adicionar(5642)(8749));
*/


/*
//para saber se o numero é divisel
const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor,20));
console.log(is_divisivel(divisor,11));
console.log(is_divisivel(divisor,13));*/


/*const diviselPor = divisor => numero =>!(numero % divisor);
const is_divisel = diviselPor(2);
console.log(is_divisel(20));
console.log(is_divisel(11));
console.log(is_divisel(13));*/



/*

Exercício 05

Escreva uma função imprimirBRL que recebe um número flutuante (ex: 4.651) 
e retorne como saída uma string no seguinte formato (seguindo o exemplo): “R$ 4,66”

Outros exemplos:

imprimirBRL(0) // “R$ 0,00”
imprimirBRL(3498.99) // “R$ 3.498,99”
imprimirBRL(-3498.99) // “-R$ 3.498,99”
imprimirBRL(2313477.0135) // “R$ 2.313.477,02”

OBS: note que o arredondamento sempre deve ser feito para cima, em caso de estourar a precisão de 
dois números
OBS: o separador decimal, no Brasil, é a vírgula
OBS: o separador milhar, no Brasil, é o ponto




function imprimirBRL(valor){
    var valorArredondado = Math.abs(valor.toFixed(2));
    return console.log(`${valor > 0 ? '' : '-' }R$ ${valorArredondado});
}

valor=prompt("digite o valor");

imprimirBRL(valor);
*/



/*

Escreva uma função imprimirGBP que recebe um número flutuante (ex: 4.651)
e retorne como saída uma string no seguinte formato (seguindo o exemplo): “£ 4.66”

Outros exemplos:

imprimirGBP(0) // “£ 0.00”
imprimirGBP(3498.99) // “£ 3,498.99”
imprimirGBP(-3498.99) // “-£ 3,498.99”
imprimirGBP(2313477.0135) // “£ 2,313,477.02”

OBS: note que o arredondamento sempre deve ser feito para cima, em caso de estourar a precisão de dois números
OBS: o separador decimal, no Reino Unido, é o ponto
OBS: o separador milhar, no Reino Unido, é a vírgula

*/






/*

Escreva uma função imprimirFR que recebe um número flutuante (ex: 4.651) e retorne como saída uma string no seguinte formato (seguindo o exemplo): “4,66 €”

Outros exemplos:

imprimirFR(0) // “0,00 €”
imprimirFR(3498.99) // “3.498,99 €”
imprimirFR(-3498.99) // “-3.498,99 €”
imprimirFR(2313477.0135) // “2.313.477,02 €”

OBS: note que o arredondamento sempre deve ser feito para cima, em caso de estourar a precisão de dois números
OBS: o separador decimal, na França, é a vírgula
OBS: o separador milhar, na França, é o ponto


*/

/*Escreva uma função imprimirGBP que recebe um número flutuante (ex: 4.651) 
e retorne como saída uma string no seguinte formato (seguindo o exemplo): “£ 4.66”

Outros exemplos:

imprimirGBP(0) // “£ 0.00”
imprimirGBP(3498.99) // “£ 3,498.99”
imprimirGBP(-3498.99) // “-£ 3,498.99”
imprimirGBP(2313477.0135) // “£ 2,313,477.02 

var numero = 4.66

function imprimirGBP (numero){
    var valorArredondado = Math.abs(numero.toFixed(2));
    return console.log(`${numero > 0 ? '' : '-' }£ ${valorArredondado}`);
}

imprimirGBP(-4.66,25)/*

/*Escreva uma função imprimirFR que recebe um número flutuante (ex: 4.651) e retorne como saída uma string no seguinte formato (seguindo o exemplo): “4,66 €”

Outros exemplos:

imprimirFR(0) // “0,00 €”
imprimirFR(3498.99) // “3.498,99 €”
imprimirFR(-3498.99) // “-3.498,99 €”
imprimirFR(2313477.0135) // “2.313.477,02 €”

OBS: note que o arredondamento sempre deve ser feito para cima, em caso de estourar a precisão de dois números
OBS: o separador decimal, na França, é a vírgula
OBS: o separador milhar, na França, é o ponto */


var numero = 4.66

function imprimirGBP (numero){
    var valorArredondado = Math.abs(numero.toFixed(2));
    return console.log(`${numero > 0 ? '' : '-' } ${valorArredondado} ¢`);
}

//imprimirGBP(-4.66,25)

function imprimirBRL (numero){
	
	numero.toFixed(2).replace('.',',')
	return numero.toLocaleString('pr-br', {style: 'currency',currency:'EUR'} )
	
}

//console.log(imprimirBRL(1520.68)) 




