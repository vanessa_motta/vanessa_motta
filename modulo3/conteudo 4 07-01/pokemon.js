// API endpoint
const baseUrl = 'https://pokeapi.co/api/v2/pokemon/';

function getElement( element ) {
  return document.querySelector( element );
}

// Get Elements
const searchInput = getElement( '.search-input' );
const section = getElement( '.pokemon' );
const erroMessage = getElement( '.error' );
const especial = getElement( '.especial' );

let pokeID; // numero passado na caixa de busca
// Responsavel por guardar os dados recebidos da API
let pokeAltura;
let pokePeso;
let pokeTipo;
let pokeEstatisticas;
let pokeImagem;
let pokeName;
let card; // Responsavel por receber o HTML

let erro = false;
/* fetch(baseUrl+'1')
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(err => console.log(err)); */

function createCard() {
  card = `
        <div class="pokemon-picture">
          <img src="${ pokeImagem }" alt="Sprite of ${ pokeName }">
        </div>
        <div class="pokemon-info">
            <h3 class="name">Nome: ${ pokeName }</h1>
            <h3 class="number">Número ID: ${ pokeID }</h2>
            <h3 class="type">Tipo: ${ pokeTipo.map( item => ` ${ item.type.name }` ).toString() }</h3>
            <h3 class="weight">Peso: ${ pokePeso }kg</h3>
            <h3 class="height">Altura: ${ pokeAltura }m</h3>
            <h3 class="type">Características: ${ pokeEstatisticas.map( item => ` ${ item.stat.name }` ).toString() }</h3>          
        </div>`;

  return card;
}


function getPokemon( event ) {
  event.preventDefault();

  pokeID = searchInput.value.toLowerCase();

  fetch( baseUrl + pokeID )
    .then( ( response ) => {
      if ( response.status !== 200 ) {
        // make the promise be rejected if we didn't get a 200 response
        erro = true;
        throw new Error( 'Not 200 response' )
      } else {
        response.json()
          .then( ( data ) => {
            console.log( data );
            pokeImagem = data.sprites.front_default;
            pokeAltura = data.height / 10;
            pokePeso = data.weight / 10;
            pokeTipo = data.types;
            pokeEstatisticas = data.stats;
            pokeName = data.name;
            pokeID = data.id;
            erro = false;
          } );
      }
    } )
    .catch( ( err ) => {
      console.error( 'Failed retrieving information', err );
      erro = true;
    } );


  setTimeout( () => {
    // Exibe uma mensagem caso o pokemon pesquisado não exista

    if ( erro ) { // se trver erro mostra erro
      erroMessage.style.display = 'block';
      section.style.display = 'none';     
    } else { // senão mostra o pokemon
      erroMessage.style.display = 'none';
      section.style.display = 'flex';
      section.innerHTML = createCard();
    }
  }, 2000 );
}

function sorte() {
  const numeroMagico = Math.round( Math.random() * 802 );
  fetch( baseUrl + numeroMagico )
    .then( ( response ) => {
      if ( response.status !== 200 ) {
        // make the promise be rejected if we didn't get a 200 response
        erro = true;
        throw new Error( 'Not 200 response' )
      } else {
        response.json()
          .then( ( data ) => {
            console.log( data );
            pokeImagem = data.sprites.front_default;
            pokeAltura = data.height / 10;
            pokePeso = data.weight / 10;
            pokeTipo = data.types;
            pokeEstatisticas = data.stats;
            pokeName = data.name;
            pokeID = data.id;
            erro = false;
          } );
      }
    } )
    .catch( ( err ) => {
      console.error( 'Failed retrieving information', err );
      erro = true;
    } );


  setTimeout( () => {
    // Exibe uma mensagem caso o pokemon pesquisado não exista
    if ( erro ) { // se trver erro mostra erro
      erroMessage.style.display = 'block';
      section.style.display = 'none';      
    } else { // senão mostra o pokemon
      erroMessage.style.display = 'none';
      section.style.display = 'flex';
      section.innerHTML = createCard();
    }
  }, 2000 );
}

searchInput.addEventListener( 'blur', getPokemon );// escuta quando o input perde o foco

especial.addEventListener( 'click', sorte );
