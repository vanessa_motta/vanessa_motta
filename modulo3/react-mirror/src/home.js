import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import EpisodioUi from './components/episodioUi';

class Home extends Component {  
  constructor( props ){
    super( props )
    this.listaEpisodios = new ListaEpisodios();
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      deveExibirMensagem:false, 
    }
  }  

  sortear(){
    const episodio = this.listaEpisodios.episodiosAleatorios
    //this.sortear = this.sortear.bind ( this)
    this.setState( {
      episodio
    })
  }

  assistido(){
    const{ episodio } = this.state
    //this.listaEpisodios.marcarComoAssistido( episodio )
    episodio.marcarParaAssistido()
    this.setState({
      episodio      
    })
  }
 
  registrarNota( evento ){
    const { episodio } = this.state
    episodio.avaliar( evento.target.value)
    this.setState({
      episodio,
      deveExibirMensagem: true
    })    
    setTimeout(() => {
      this.setState({
        deveExibirMensagem: false
      })
    }, 5000);
  }

  geraCampoDeNota(){
    return(
      <div>
        {
          this.state.episodio.assistido && (
            <div>
              <span>Avalie este episódio</span>  
              <input  type="number" placeholder="1 a 5" min="1" max="5" onBlur={ this.registrarNota.bind(this)}         />
            </div>
          )
        }
      </div>         
    )
  }

  render(){ 
    const { episodio, deveExibirMensagem } = this.state;

    return (
      <div className="App">   
        {/*{ deveExibirMensagem ? (<span>Registramos sua nota!</span>) : '' */}
         <span className={`flash verde ${ deveExibirMensagem ? '' : 'invisivel'}`}>Registramos sua nota!</span>   
         <header className='App-header'>
            <EpisodioUi episodio={ episodio } />    
            <div className="botoes">
            <button className="btn verde" onClick={ this.sortear.bind( this ) }>Próximo</button>
            <button className="btn azul" onClick={ this.assistido.bind( this ) }>Já Assisti</button>
          </div>
          { this.geraCampoDeNota() }
        </header>
      </div>
    );
  }
}

export default Home;
