import React, { Component } from 'react';

export default class EpisodioUi extends Component {
    render() {
        const { episodio } = this.props
        return(
            <React.Fragment>
                <h2>{ episodio.nome }</h2>
                <img src={ episodio.url } alt={ episodio.nome }></img>
                <span>Assistiu? { episodio.assistido ? 'Sim' : 'Não' }, { episodio.qtdVezesAssistido } vez(es) </span>
                <span>Temporada - Episódio: { episodio.temporadaEpisodio }</span>
                <h4>Duração:{ episodio.duracaoEmMinutos }</h4>     
                <h4>Nota:{ episodio.nota }</h4>  
            </React.Fragment>
        )
    }
}