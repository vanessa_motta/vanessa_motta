import React, { Component } from 'react';
import Imagem from './imagem';
import Paragrafo from './pagrafo';

export default class Artigo extends Component { 
    render()  {    
        return (
            <React.Fragment>
              <article>
                  <Imagem/>
                  <Paragrafo/>
              </article>
            </React.Fragment>
        )
    }
}