import React, { Component } from 'react';
import '../css/reset.css';
import '../css/header.css';
import '../css/grid.css';
import '../css/banner.css';
import '../css/buttons.css';
import '../css/box.css';
import '../css/footer.css';
import '../css/work.css';
import '../css/general.css';
import Logo from '../imagens/logo-dbc-topo.png'
import { Link } from 'react-router-dom';

export default class Header extends Component { 
    render()  {
      return (

        <header className="main-header">
        <nav className="container clearfix">
            <a className="logo" href="index.html" title="Voltar à home">
                <img src={ Logo } alt="DBC Company"/>
            </a>

            <label className="mobile-menu" for="mobile-menu">
                <span></span>
                <span></span>
                <span></span>                
            </label>
            <input id="mobile-menu" type="checkbox"/>

            <ul className="clearfix">
                <li>
                    <Link to="home.html">Home</Link>
                </li>
                <li>
                    <Link to="sobrenos.html">Sobre nós</Link>
                </li>
                <li>
                    <Link to="servicos.html">Serviços</Link>
                </li>
                <li>
                    <Link to="contato.html">Contato</Link>
                </li>
            </ul>
        </nav>
    </header>
      )             
    }
}

