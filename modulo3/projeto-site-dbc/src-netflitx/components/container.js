import React, { Component } from 'react';
import '../css/banner.css';
import '../css/buttons.css';


export default class Container extends Component { 
    render()  {
      return (
        <section className="container">
            <div className="row">
                <article className="col col-7">
                    <h2>Título</h2>
                    <p>
                        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                    </p>    
                </article>
                <article className="col col-5">
                    <h3>Título</h3>
                    <p>    
                    
                        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                    </p>    
                </article>                
            </div>
        </section>      
       )
    }
} 