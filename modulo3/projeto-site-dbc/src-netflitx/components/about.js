import React, { Component } from 'react';
import '../css/reset.css';
import '../css/header.css';
import '../css/grid.css';
import '../css/banner.css';
import '../css/buttons.css';
import '../css/box.css';
import '../css/footer.css';
import '../css/work.css';
import '../css/general.css';
import Agil from './imagem/agil.png';



export default class About extends Component { 
    render()  {
      return (
            <React.Fragment>
                <section className="container about-us">
                        <div className="row">
                            <article className="col col-12">
                                <img src= { Agil } alt="agil"/>
                                <h2>Título</h2>
                                <p>
                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque.
                                </p>
                            </article>
                        </div>

                        <div className="row">
                            <article className="col col-12">
                                <img src= { Agil } alt="agil"/>
                                <h2>Título</h2>
                                <p>
                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque.
                                </p>
                             </article>
                        </div>

                        <div className="row">
                            <article className="col col-12">
                                <img src= { Agil } alt="agil"/>
                                <h2>Título</h2>
                                <p>
                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque.
                                </p>
                            </article>
                        </div>

                        <div className="row">
                            <article className="col col-12">
                                <img src= { Agil } alt="agil"/>
                                <h2>Título</h2>
                                <p>
                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque.
                                </p>
                            </article>
                        </div>
                </section>             
            </React.Fragment>              
        )
    }
} 
