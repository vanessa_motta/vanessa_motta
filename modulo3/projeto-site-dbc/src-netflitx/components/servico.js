import React, { Component } from 'react';
import '../css/reset.css';
import '../css/header.css';
import '../css/grid.css';
import '../css/banner.css';
import '../css/buttons.css';
import '../css/box.css';
import '../css/footer.css';
import '../css/work.css';
import '../css/general.css';
import Agil from './imagem/agil.png';



export default class Servico extends Component { 
    render()  {
      return (
            <React.Fragment>
                <section className="container work"/>
                    <div className="row"/>
                        <div className="col col-4">
                            <article className="box">
                                <div>/
                                    <img src= { Agil } alt="agil"/>
                                </div>
                                <h2>Título</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis et at perferendis voluptatum laudantium eligendi quas tempora iure saepe a totam quo, nam doloribus architecto voluptates labore? Accusantium, voluptas velit.
                                </p>
                            </article>
                        </div>

                        <div className="col col-4">
                            <article className="box">
                                <div>
                                    <img src= { Agil } alt="agil"/>
                                </div>
                                <h2>Título</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis et at perferendis voluptatum laudantium eligendi quas tempora iure saepe a totam quo, nam doloribus architecto voluptates labore? Accusantium, voluptas velit.
                                </p>
                            </article>
                        </div>

                        <div className="col col-4">
                            <article className="box">
                                <div>
                                    <img src= { Agil } alt="agil"/>
                                </div>
                                <h2>Título</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis et at perferendis voluptatum laudantium eligendi quas tempora iure saepe a totam quo, nam doloribus architecto voluptates labore? Accusantium, voluptas velit.
                                </p>
                            </article>
                        </div>                        
                    </div>

                    <div classNames="row">
                        <div className="col col-4">
                            <article className="box">
                                <div>
                                    <img src= { Agil } alt="agil"/>
                                </div>
                                <h2>Título</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis et at perferendis voluptatum laudantium eligendi quas tempora iure saepe a totam quo, nam doloribus architecto voluptates labore? Accusantium, voluptas velit.
                                </p>
                            </article>
                        </div>

                        <div className="col col-4">
                            <article className="box">
                                <div>
                                    <img src= { Agil } alt="agil"/>
                                </div>
                                <h2>Título</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis et at perferendis voluptatum laudantium eligendi quas tempora iure saepe a totam quo, nam doloribus architecto voluptates labore? Accusantium, voluptas velit.
                                </p>
                            </article>
                        </div>

                        <div cclassName="col col-4">
                            <article className="box">
                                <div>
                                    <img src= { Agil } alt="agil"/>
                                </div>
                                <h2>Título</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis et at perferendis voluptatum laudantium eligendi quas tempora iure saepe a totam quo, nam doloribus architecto voluptates labore? Accusantium, voluptas velit.
                                </p>
                            </article>
                        </div>
                    </div>                    
                </section>             
            </React.Fragment>              
        )
    }
} 
