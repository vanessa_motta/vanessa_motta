import React, { Component } from 'react';
import '../css/reset.css';
import '../css/header.css';
import '../css/grid.css';
import '../css/banner.css';
import '../css/buttons.css';
import '../css/box.css';
import '../css/footer.css';
import '../css/work.css';
import '../css/general.css';
import '../css/contact.css';
import '../css/about-us.css';
import Mapa from './contato/mapa';


export default class Form extends Component { 
    render()  {
      return (
            <React.Fragment>
                <section className="container work">    
                    <div className="row">
                        <div className="col col-6">
                            <h2>Título</h2>
                            <p>
                                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fugiat molestiae harum ex delectus. Placeat doloribus illum aliquid, similique a vel perspiciatis modi porro explicabo quae. Dicta consequatur esse explicabo minus.
                            </p>

                            <form className="clearfix">
                                <input className="field" type="text" placeholder="Nome"/>
                                <input className="field" type="text" placeholder="E-mail"/>
                                <input className="field" type="text" placeholder="Assunto"/>
                                <textarea className="field" placeholder="Mensagem"></textarea>
                                <input className="button button-green button-right" type="submit" value="Enviar"/>
                            </form>
                        </div>
                    </div>

                    <div className="map-container">
                       <iframe className="map" src={ Mapa } allowfullscreen=""></iframe>
                    </div>
                </section>                 
            </React.Fragment>              
        )
    }
} 
