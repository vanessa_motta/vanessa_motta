import React, { Component } from 'react';
import '../css/grid.css';
import '../css/buttons.css';
import Smart from '../imagens/smart.png';
import Builder from '../imagens/builder.png';
import Sustain from '../imagens/sustain.png';
import Agil from '../imagens/agil.png';




export default class Box extends Component { 
    render()  {
      return (
            <React.Fragment>             

                <div className="col col-12 col-md-6 col-lg-3">
                    <article className="box">
                        <div>
                        <img src={ Smart} alt='sustain'/>
                        </div>
                        <h4>Título</h4>
                        <p>
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                        </p>
                        <button className="button button-blue">Saiba mais</button>
                    </article>
                </div>

                <div className="col col-12 col-md-6 col-lg-3">
                    <article className="box">
                        <div>
                        <img src={ Builder } alt='builder'/>               
                        </div>
                        <h4>Título</h4>
                        <p>
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                        </p>
                        <button className="button button-blue">Saiba mais</button>
                    </article>
                </div>
            
                <div className="col col-12 col-md-6 col-lg-3">
                    <article className="box">
                        <div>
                        <img src={ Sustain} alt='sustain'/>
                        </div>
                        <h4>Título</h4>
                        <p>
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                        </p>
                        <button className="button button-blue">Saiba mais</button>
                    </article>
                </div>

                <div className="col col-12 col-md-6 col-lg-3">
                    <article className="box">
                        <div>
                        <img src={ Agil } alt='agil'/>
                        </div>
                        <h4>Título</h4>
                        <p>
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                        </p>
                        <button className="button button-blue">Saiba mais</button>
                    </article>
                </div>                          
            </React.Fragment>    
        )
    }  
} 