import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import '../css/reset.css';
import '../css/header.css';
import '../css/grid.css';
import '../css/banner.css';
import '../css/buttons.css';
import '../css/box.css';
import '../css/footer.css';
import '../css/work.css';
import '../css/general.css';


export default class Footer extends Component { 
    render()  {
      return (
            <React.Fragment>
                <footer class="main-footer">
                    <div class="container">
                        <nav>
                            <ul>
                            <li>
                                <Link to="../pages/home">Home</Link>
                            </li>
                            <li>
                                <Link to="sobrenos.html">Sobre nós</Link>
                            </li>
                            <li>
                                <Link to="servicos.html">Serviços</Link>
                            </li>
                            <li>
                                <Link to="contato.html">Contato</Link>
                            </li>
                            </ul>
                        </nav>
                        <p>
                            &copy; Copyright DBC Company - 2019
                        </p>
                    </div>
                </footer>                
            </React.Fragment>              
        )
    }
} 

