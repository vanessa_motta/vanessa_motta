import React, { Component } from 'react';
import '../css/banner.css';
import '../css/buttons.css';


export default class Header extends Component { 
    render()  {
      return (
        <section className="main-banner">
            <article>
                <h1>Vem ser DBC</h1>
                <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus.
                </p>
                <button className="button button-big button-outline">Saiba mais</button>
            </article>
        </section>
      )
    }  
} 