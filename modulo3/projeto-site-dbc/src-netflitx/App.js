import React, { Component } from 'react';
import './App.css';
import Header from './components/header';
import Banner from './components/banner';
import Container from './components/container';
import Box from './components/box';
import Footer from './components/footer';
import About from './components/about';
import Servico from './components/servico';
import Contato from './components/contato';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

export default class App extends Component {
  render() {
    return (
      <Router>
        <Route path="/" exact component={Home} />
        <Route path="/sobre-nos" component={Sobre}/> 
        <Route path="/servicos" component={Servicos}/>
        <Route path="/contato" component={Contato}/>

      </Router>

    );
  }
}

const Home = () =>(
   <div className="App">
      <Header />
      <Banner />
      <Container />
      <Box />
      <Footer /> 
    </div>
)
 
const Sobre = () =>(
  <div className="App">
      <Header />
      <About />
      <Footer />
  </div>
)

const Servicos = () => (
  <div className="App">
      <Header />
      <Servico />
      <Footer />
  </div>    
)

const Contato = () => (
  <div className="App">
    <Header />
    <Footer />
  </div>
)