import React, { Component } from 'react';
import './App.css';
import Header from './components/header';
import Servico from './components/servico';
import Footer from './components/footer';




export default class Home extends Component { 
    render()  {    
        return (
            <React.Fragment>
              <article>
                  <Header/>
                  <Servico/>
                  <Footer/>
              </article>
            </React.Fragment>
        )
    }
}
