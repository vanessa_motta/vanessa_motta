import React, { Component } from 'react';
import './App.css';
import Header from './components/header';
import About from './components/about';
import Footer from './components/footer';



export default class Sobre extends Component { 
    render()  {    
        return (
            <React.Fragment>
              <article>
                  <Header/>
                  <About/>
                  <Footer/>
              </article>
            </React.Fragment>
        )
    }
}
