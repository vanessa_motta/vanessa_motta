import React, { Component } from 'react';
import './App.css';
import Header from './components/header';
import Banner from './components/banner';
import Container from './components/container';
import Box from './components/box';
import Footer from './components/footer';


export default class Home extends Component { 
    render()  {    
        return (
            <React.Fragment>
              <article>
                  <Header/>
                  <Banner/>
                  <Container/>
                  <Box/>
                  <Footer/>
              </article>
            </React.Fragment>
        )
    }
}
