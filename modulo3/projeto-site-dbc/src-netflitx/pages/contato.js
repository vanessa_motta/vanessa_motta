import React, { Component } from 'react';
import './App.css';
import Header from './components/header';
import Form from './components/form';
import Footer from './components/footer';



export default class Contato extends Component { 
    render()  {    
        return (
            <React.Fragment>
              <article>
                  <Header/>
                  <Form/>                  
                  <Footer/>
              </article>
            </React.Fragment>
        )
    }
}
