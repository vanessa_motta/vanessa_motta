

var nomeDaVariavel = "valor";
let nomeDaLet = "valorlet";
const nomeDaConst = "ValorDaConst"

var nomeDaVariavel = 'valor3';
nomeDaLet = "valorDaLet2";

const nomeDaConst2 = {
    nome: "marcos",
    idade: 29
};

Object.freeze(nomeDaConst2);

nomeDaConst2.nome = "Marcos Henrique";

//console.log(nomeDaConst2);
//comsole.log(nomeDaLet);

function nomeDaFunction(){
    let nomeDaLet ="ValorDaLet2";
}

/*function(){

}*/


function somar(valor1, valor2=1){
    console.log(valor1 + valor2);
}

//somar(3);
//somar(3, 2);

/*console.log(1+1);
console.log(1 + "1");*/

function ondeMoro(cidade){
    //console.log("eu moro em " + cidade + ". E sou feliz!");
    console.log(`eu moro em ${cidade}. E sou feliz ${30+1} dias`);

}

//ondeMoro("Gravataí");

function fruteira(){
    let texto = "Banana"
                + "\n"
                +"Amaeixa"
                +"\n"
                +"Pêssego"
                +"\n";
    let newTexto = `
        Banana
            Ameixa
                 Goiaba
                      Pêssego
                 `;
                 
    console.log(texto);
    console.log(newTexto);             

}

//fruteira();

let eu = {
    nome:"Marcos",
    idade:29,
    altura:1.85
};

function quemSou(pessoa){
    console.log(`Meu nome é ${pessoa.nome}, tenho ${pessoa.idade} anos e ${pessoa.altura} de altura`);
}
 
///quemSou(eu);

let funcaoSomarValores = function(a,b){
    return a + b;
}

let add = funcaoSomarValores
let resultado = add(3,5)

//console.log(resultado); 

const{nome:n, idade:i} = eu;
// console.log(nome,idade);
//console.log(n,i);

const array = [1,3,4,8];
const [n1, ,n3, n2, n4 = 18] = array
//console.log(n1,n2,n3, n4);

function testarPessoa({nome,idade,altura}){
    console.log(nome,idade,altura);
}

//testarPessoa(eu);

let a1=97;
let b1=98;
let c1=99;
let d1=100;

[a1,b1,c1,d1] = [b1,d1,a1,c1];
console.log(a1,b1,c1,d1);