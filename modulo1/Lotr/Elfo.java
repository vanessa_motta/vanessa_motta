  
public class Elfo extends Personagem {
    
     private int indiceFlecha; 
     privte static int qtdElfos;
     //private String nome;
     //private int experiencia;
     //private Inventario mochila;
     //private Status status;
     //private Dwarf vida = new Dwarf();
     
     {
         indiceFlecha = 0;
        //experiencia = 0;
        //Inventario mochila = new Inventario();
        //Status status = Status.RECEM_CRIADO;        
        }
     
     
     public Elfo( String nome){
        super(nome);
        this.vida = 100.0;        
        this.ganharItem(new Item(2,"Flecha"));
        this.ganharItem(new Item(1,"Arco"));
        Elfo.qtdElfos++;    
     }
     
     protected void finalize()throws Throwable{
         Elfo.qtdElfos--;
        }
        
     public static int getQtdElfos(){
        return Elfo.qtdElfos;
     }
        
     public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
      }
                  
     public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
     }   
        
     public boolean podeAtirarFlecha(){
        return this.getQtdFlecha()>0;
     }
     
     public void atirarFlecha( Dwarf dwarf ){
        if( podeAtirarFlecha() ) {
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            //this.experiencia = experiencia + 1;
            this.aumentarXp();
            dwarf.sofrerDano();
            this.sofrerDano();
        }
     }
    
     public void aumentarXp(){
         experiencia++; 
     }
               
     /*public void atirarFlecha( Dwarf dwarf){
        
         if( podeAtirarFlecha()){
             this.getFlecha().setQuantidade( this.getQtdFlecha() -1);
             this.aumentarXp();
             dwarf.sofrerDano(); 
        }*/    
         

        
    /*public String getNome() {
        return this.nome; 
     }*/
    
     
     /*public Status getStatus() {
        return this.status; 
     }*/
    
     
     /*public void setNome( String nome ){
        this.nome = nome;
     }*/
    
     /*public int getExperiencia () {
        return this.experiencia;
     } */  
     
     /*public void setExperiencia(int experiencia){
         this.experiencia += experiencia;
     }*/
}  


