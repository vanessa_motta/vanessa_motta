import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoVerdeTest{
    
    @Test
    public void elfoVerdeGanha2xPorFlecha(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        celebron.atirarFlecha(new Dwarf("Balin"));
        assertEquals(2, celebron.getExperiencia());
    }
      
    
    public void elfoVerdeAdicionarItemComDescricaoValida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoVidro = new Item(1, "Arco de Vidro");
        celebron.ganharItem(arcoVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1, "Arco"),inventario.obter(0));
        assertEquals(new Item(2, "Flecha"), inventario.obter(1));
        assertEquals(arcoVidro, inventario.obter(2));
    }
    
    public void elfoVerdePerdeItemComDescricaoValida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoVidro = new Item(1, "Arco de Vidro");
        celebron.ganharItem(arcoVidro);
        celebron.perderItem(arcoVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1, "Arco"),inventario.obter(0));
        assertEquals(new Item(2, "Flecha"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de Vidro"));
    }
    
    public void elfoVerdeAdicionarItemComDescricaoInvalida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoVidro = new Item(1, "Arco de madeira");
        celebron.ganharItem(arcoVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(1, "Arco"),inventario.obter(0));
        assertEquals(new Item(2, "Flecha"), inventario.obter(1));
        assertNull(inventario.buscar("Arco de madeira"));
    }
    
    public void elfoVerdePerdeItemComDescricaoInvalida(){
        ElfoVerde celebron = new ElfoVerde("Celebron");
        Item arcoVidro = new Item(1, "Arco");
        celebron.perderItem(arcoVidro);
        Inventario inventario = celebron.getInventario();
        assertEquals(new Item(2, "Flecha"),inventario.obter(0));
        assertEquals(new Item(1, "Arco"), inventario.obter(1));
        assertEquals(arcoVidro, inventario.obter(2));
    }

}