/* Os Dwarves finalmente poderão se defender! Agora um Dwarf tem um inventário e ganha um item escudo (uma unidade) ao nascer.
Ao equipar o escudo (isso é feito de forma manual, ou seja, é preciso fazer o dwarf equipá-lo), o dwarf passa a tomar metade 
do dano (em cada chamada).*/

public class Dwarf extends Personagem{

    //private String nome;
    //private double vida;
    //private Status status;
    //private Inventario inventario; // primeira coisa a fazer para criar um item ao personagem
    //private boolean equipado = false; 
       

    public Dwarf(String nome){
        super(nome);        
        this.vida = 110.0;
        this.qtdDano = 10.0;
        this.ganharItem(new Item(1, "Escudo")); // para adicionar um item ao personagem
    }

    public void equiparEscudo(){
        this.qtdDano = 5.0;
    }
        
    /*public double calcularDano(){
        return this.equipado ? 5.0 : 10.0;
    }*/
    
    /*public void sofrerDano() {
        if( this.podeSofrerDano() ) {
            //comparacao ? verdadeiro : falso;
            this.vida = this.vida >= this.calcularDano() ?
            this.vida - this.calcularDano() : 0.0;
            
            if( this.vida == 0.0 ){
                this.status = Status.MORTO;
            }else{
                this.status = Status.SOFREU_DANO;
            }
        }
    }*/
             
        
    /*public void ganharItem (Item item){
        this.inventario.adicionar(item);
    }*/
    
    
    /*public void perderItem(Item item){
        this.inventario.remover(item);
    }*/
    
    
    /*public double calcularDano(){
        return this.equipado ? 5.0 : 10.0; //? = se e : = senao
    }*/
    
          
    /*public void equiparEscudo(){
        this.equipado = true;
    }*/
    
    /*public double getVida(){
        return this.vida;
    }*/
    
    /*public String getNome(){
        return this.nome;
    }*/
    
    /*public void setNome(String nome){
        this.nome = nome;
    }*/


 } 
    
