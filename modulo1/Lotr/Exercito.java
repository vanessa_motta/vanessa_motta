import java.util.*;

public class Exercito{
    //private ArrayList<Elfo> contingente;
    private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(Arrays.asList(ElfoVerde.class,ElfosNotunos.class));
    private ArrayList<Elfo> elfos = new ArrayList<>();
    private HashMap<Status, ArrayList<Elfo>> porStatus = new HashMap<>();

    public void alistar( Elfo elfo){
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if(podeAlistar){
            //elfo.add(elfo);
            ArrayList<Elfo> elfoDoStatus = porStatus.get( elfo.getSatus() );
            if( elfoDoStatus == null ){
                elfoDoStatus = new ArrayList<>();
                porStatus.put(elfo.getSatus(), elfoDoStatus);
            }
            elfoDoStatus.add(elfo);
        }
    }
    
    public ArrayList <Elfo> getElfos(){
        return this.elfos;
    }
    
    public ArrayList<Elfo> buscar( Status status ){
        return this.porStatus.get(status);
    }
    
    public ArrayList<Elfo> buscar( Status status ){
        return this.porStatus.get(status);
    }
    
    /*public Exercito()
    {
        this.contingente = new ArrayList<>();
    }*/


    /*public void recrutar(Elfo elfo)
    {
        if(elfo instanceof ElfoVerde || elfo instanceof ElfosNotunos)
        {
            this.contingente.add(elfo);
        }
    }*/
    
    /*public ArrayList<Elfo> buscaPorStatus(Status status)
    {
        ArrayList<Elfo> resultadoBusca = new ArrayList<>();
        for(int i = 0; i < this.contingente.size(); i++){
            if(this.contingente.get(i).getSatus() == status){
              resultadoBusca.add(this.contingente.get(i));  
            }
        }
        return resultadoBusca;
    }*/
}
