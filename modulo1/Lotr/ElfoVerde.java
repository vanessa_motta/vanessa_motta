import java.util.ArrayList;
import java.util.Arrays;

public class ElfoVerde extends Elfo {
          
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>
        (Arrays.asList("Espada de aço valiriano","Arco de vidro","Flecha de vidro")); 
        
    public ElfoVerde( String nome ){
        super(nome);
        this.qtdExperienciaPorAtaque = 2;
    }    
    
    @Override
    public void ganharItem( Item item ){
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if (descricaoValida) {
            this.inventario.adicionar(item);
        }
    } 
     
    @Override
    public void perderItem( Item item ){
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if (descricaoValida) {
                this.inventario.remover(item);
         }
    }
    
    
     
           
    /*public ElfoVerde( String nome){
        super(nome);
        //this.ganharItem(new Item(2,"Flecha de vidro"));
        //this.ganharItem(new Item(1,"Arco de vidro"));
     }*/
     
     
    /*public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
      }*/
                  
    /*public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
     }*/   
         
    /*public void aumentarXp(){
         experiencia = experiencia + qtdExperienciaPorAtaque;
     }*/
     
    /*public boolean podeAtirarFlecha(){
        return this.getQtdFlecha()>0;
     }*/
     
   
    /* public void ganharItem(Item item){
        if ( item.getDescricao().compareTo("Flecha de vidro") == 0 ||
             item.getDescricao().compareTo("Arco de vidro") == 0 ||
             item.getDescricao().compareTo("Espada de aço valiriano") == 0){
                 
             this.inventario.adicionar(item);    
         }
                 
     }*/
     
     
     /*public void perderItem(Item item){
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(item.getDescricao());
        if (descricaoValida){
                this.inventario.remover(item);
        }
        
     }*/
    
     /*public void atirarFlecha( Dwarf dwarf){
        
         if( podeAtirarFlecha()){
             this.getFlecha().setQuantidade( this.getQtdFlecha() -1);
             this.aumentarXp();
             dwarf.sofrerDano(); 
        }    */
         
}

