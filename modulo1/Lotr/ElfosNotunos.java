import java.util.ArrayList;
import java.util.Arrays;

// Elfos Noturnos ganham o triplo de experiência de um Elfo normal ao atirar uma flecha, 
//mas perdem 15 unidades de vida para cada flecha atirada.

public class ElfosNotunos extends Elfo {
  
  public ElfosNotunos( String nome ){
        super(nome);
        this.qtdExperienciaPorAtaque = 3;
        this.qtdDano = 15.0;
    }   
    
  /*public double calcularDano(){
        return this.equipado ? 5.0 : 10.0;
    }*/  
    
  public void atirarFlecha( Dwarf dwarf ){
        if( podeAtirarFlecha() ) {
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            //this.experiencia = experiencia + 1;
            this.aumentarXp();
            dwarf.sofrerDano();
        }
  }
    
}

