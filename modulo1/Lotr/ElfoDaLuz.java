import java.util.*;

public class ElfoDaLuz extends Elfo{
   private int qtdAtaques;
   private final double QTD_VIDA_GANHA = 10;
   private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(Array.asList("Espada de Galvorn"));
   private int numeroAtaque;
   
   {
       qtdAtaques = 0;
   }

    public ElfoDaLuz(String nome)
    {
        super(nome);
        this.qtdDano = 21.0;
        super.ganharItem(new ItemSempeExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
        //this.inventario.adicionar(new Item(1,"Espada de Galvorn"));
    }

    private boolean devePerderVida(){
        return qtdAtaques % 2 ==1;
    }
    
    public void ganharVida(){
        vida += QTD_VIDA_GANHA;
    }
    
    public void perderItem ( Item item){
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if (possoPerder){
        super.perderItem(item);
        }
    }
     
    public void atacarComEspada( Dwarf dwarf ){
        if(this.getSatus() != Status.MORTO){
            dwarf.sofrerDano();
            this.aumentarXp();
            qtdAtaques++;
            if(devePerderVida()){
                this.qtdDano = 21.0
                this.sofrerDano(); 
                this.qtdDano = 0.0;
            }else{
                this.ganharVida();
            }
        }
    }
    /*public void atacarComEspada(Dwarf dwarf)
    {
       numeroAtaque++;
       if(numeroAtaque % 2 == 0){//testa se o ataque é par
           this.aumentarXp();
           dwarf.sofrerDano();
           this.vida += 10;
       }else{
           this.aumentarXp();
           dwarf.sofrerDano();
           this.qtdDano = 21.0;
           this.sofrerDano();
       }
    }*/
    
} 

