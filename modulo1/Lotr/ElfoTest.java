
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoTest{
    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXP(){
    
        Elfo novoElfo = new Elfo("Giallous");
        Dwarf novoDwarf = new Dwarf("Fetucius");
        //novoElfo.atirarFlecha( novoDwarf );
        assertEquals(1, novoElfo.getExperiencia());
        //assertEquals(1, novoElfo.getQtdFlecha());
    }
 
    @Test
    public void atirar2FlechaDiminuirFlechaAumentarXP(){
    
        Elfo novoElfo = new Elfo("Giallous");
        Dwarf novoDwarf = new Dwarf("Fetucius");
        //novoElfo.atirarFlecha( novoDwarf );
        //novoElfo.atirarFlecha( novoDwarf );
        assertEquals(2, novoElfo.getExperiencia());
        //assertEquals(0, novoElfo.getQtdFlecha());
    }
    
    @Test
    public void atirar3FlechaDiminuirFlechaAumentarXP(){
    
        Elfo novoElfo = new Elfo("Giallous");
        Dwarf novoDwarf = new Dwarf("Fetucius");
        //novoElfo.atirarFlecha( novoDwarf );
        //novoElfo.atirarFlecha( novoDwarf );
        //novoElfo.atirarFlecha( novoDwarf );
        assertEquals(2, novoElfo.getExperiencia());
        //assertEquals(0, novoElfo.getQtdFlecha());
    }
    
    @Test
    public void cria1ElfoContatodorUmaVez(){
        Elfo novoElfo = new Elfo("Giallous");
        asserEquals(0, Elfo.getQtdElfos());
    }
    
    @Test
    public void cria2ElfoContatodorDuasVezes(){
        Elfo novoElfo = new Elfo("Giallous");
        Elfo novoElfo1 = new Elfo("Giallous");
        asserEquals(2, Elfo.getQtdElfos());
    }
    
}

   
    