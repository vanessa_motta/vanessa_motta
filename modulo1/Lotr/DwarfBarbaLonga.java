

public class DwarfBarbaLonga extends Dwarf{
    
    private Sorteador sorteador;
    
    public DwarfBarbaLonga( String nome ){
        super(nome);
        sorteador = new DadoD6();
    }
    
    public DwarfBarbaLonga( String nome, Sorteador soerteador ){
        super(nome);
        sorteador = sorteador;
    }
    
    public void sofrerDano(){
        boolean devePerderVida = sorteador.sortear() <= 4;
        if(devePerderVida){
            super.sofrerDano();
        }
    }
    
}
