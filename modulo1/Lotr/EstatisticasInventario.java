
/* Crie uma classe chamada EstatisticasInventario, que recebe na sua construção um inventário e que possua os seguintes métodos:

calcularMedia: retorna a média de quantidades de itens do inventário.
calcularMediana: retorna a mediana das quantidades de itens do inventário.
qtdItensAcimaDaMedia: retorna a quantidade de itens que estão acima da média de quantidades*/

import java.util.*;



public class EstatisticasInventario
{
    private Inventario inventario;
    

    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }
    
    public double calcularMedia(){
        if (this.inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        
        double somaQtds = 0;
        for(Item item : this.inventario.getItens()){
        somaQtds += item.getQuantidade();
        }
        
        return somaQtds / inventario.getItens().size();
    }
     
       
    /*
    public float calcularMedia (){
        ArrayList<Item>itens=this.inventario.getItens();
        int soma = 0;
        float media = 0;
        for (int i = 0; i < itens.size();i++){
            soma += itens.get(i).getQuantidade(); 
        }
        
        media = soma/itens.size();
        
        return media;
    
    }*/
    
    /*
    public float calcularMediana(){
        ArrayList<Item>itens=this.inventario.getItens();
        
        Collections.sort(itens, new Comparator<Item>() {
            @Override
            public int compare(Item iten1, Item iten2) {
                return iten1.getQuantidade().compareTo(iten2.getQuantidade());
            }
        });
        int mediana = 0;
        //teste se a quantidade de itens da lista é par
        if(itens.size() % 2 == 0){
            //Se o número de elementos for par, então a mediana é a média dos dois valores centrais. 
            //Soma os dois valores centrais e divide o resultado por 2
            mediana = (itens.get(itens.size()/2).getQuantidade() + itens.get((itens.size()/2)+1).getQuantidade()) / 2;
        }else{ // impar
            //Se o número de elementos for ímpar, então a mediana é o valor central
            mediana = itens.get(((itens.size()-1)/2)+1).getQuantidade();//arredonda para cima
        }
        return mediana;
    }
    */
    
    public double calcularMediana(){
        if (this.inventario.getItens().isEmpty()){
            return Double.NaN;
        } 
     
    
        int qtdItens = this.inventario.getItens().size();
        int meio = qtdItens / 2;
        int qtdMeio = this.inventario.obter(meio).getQuantidade();
        if ( qtdItens % 2 == 1 ){
            return qtdMeio;
        }
    
        int qtdMeioMenosUm = this.inventario.obter( meio - 1).getQuantidade();
        return (qtdMeio + qtdMeioMenosUm ) / 2.0;
    }
    
    public int qtdItensAcimaDaMedia (){
        double media = this.calcularMedia();
        int qtdAcima = 0;
        
        for(Item item : this.inventario.getItens()){
            if (item.getQuantidade() > media ) {
            qtdAcima++;
            }
        }
        return qtdAcima;
    }
    
    
    /*
    public int qtdItensAcimaDaMedia(){
        ArrayList<Item>itens=this.inventario.getItens(); 
        
        float media = calcularMedia();
        int qtdItensAcimaDaMedia = 0;
        for (int i = 0; i < itens.size();i++){
            if(media < itens.get(i).getQuantidade()){//testa se a quantidade é maior que a media
                qtdItensAcimaDaMedia++;
            }      
        }
        return qtdItensAcimaDaMedia;
    }*/
}
