import java.util.*;

public class PaginadorInventario{
    // variáveis de instância - substitua o exemplo abaixo pelo seu próprio
    private Inventario inventario;
    private int marcador;

    public PaginadorInventario(Inventario inventario){
        // inicializa variáveis de instância
        this.inventario = inventario;
        //this.marcador = 0;
    }


    public void pular(int marcador)
    {
        this.marcador = marcador > 0 ? marcador : 0;
    }
    
        
    public ArrayList<Item> limitar(int qtd){
        /*ArrayList<Itens> lista = new ArrayList<>();
        int fim = this.marcador + qtd;
        for(int i = this.marcador; i < fim && i < this.inventario.getItens().size(); i++){
            subConjunto.add(this.inventario.obter(i));
        }
        return subConjunto;*/
        ArrayList<Item> subConjunto = new ArrayList<>();
        int fim = this.marcador + qtd;
        for ( int i = this.marcador; i < fim && i < this.inventario.getItens().size(); i++ ){
            subConjunto.add(this.inventario.obter(i));
        }
        
        return subConjunto;
    }
}
