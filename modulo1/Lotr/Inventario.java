import java.util.*;

public class Inventario{
    private ArrayList<Item> itens;
    
    public Inventario( int quantidade ) {
        this.itens = new ArrayList<Item>(quantidade);
    }
    
    public ArrayList<Item> getItens() {
        return this.itens;
    }
    
    public Item obter( int posicao ) {
        if( posicao >= this.itens.size() ) {
            return null;
        }
        return this.itens.get(posicao);
    }
    
    public void remover( Item item ) {
        this.itens.remove(item);
    }
    
    public void adicionar( Item item ){
        this.itens.add(item);
    }
    
    public String getDescricoesItens() {
        StringBuilder descricoes = new StringBuilder();
        for ( int i = 0; i < this.itens.size(); i++ ){
            Item item = this.itens.get(i);
            if ( item != null){
                descricoes.append(item.getDescricao());
                descricoes.append(",");
            }
        }
        
        return (descricoes.length() > 0 ?
        descricoes.substring(0, (descricoes.length() - 1)) :
        descricoes.toString()
        );
    }
    
    public Item getItemComMaiorQuantidade(){
        int indice = 0, maiorQuantidade = 0;
        
        for ( int i = 0; i < this.itens.size(); i++ ){
            Item item = this.itens.get(i);
            if ( item != null){
                if ( item.getQuantidade() > maiorQuantidade ) {
                    maiorQuantidade = item.getQuantidade();
                    indice = i;
                }
            }
        }
        
        return this.itens.size() > 0 ? this.itens.get(indice) : null;
    }
    
    
    public Item buscar( String descricao ){
        for ( Item itemAtual : this.itens ) {
            boolean encontrei = itemAtual.getDescricao().equals(descricao);
            if( encontrei ){
                return itemAtual;
            }
        }
        return null;
    }
    
    public ArrayList<Item> inverter(){
        ArrayList<Item> listaInvertida = new ArrayList<>(this.itens.size());
        
        for ( int i = this.itens.size() - 1; i >= 0; i--){
            listaInvertida.add(this.itens.get(i));
        }
        
        return listaInvertida;
    }
    
    public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    public void ordenarItens(TipoOrdenacao ordenacao){
        for ( int i = 0; i < this.itens.size(); i++ ){
            for ( int j = 0; j < this.itens.size() - 1; j++){
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get(j + 1);
                
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? 
                atual.getQuantidade() > proximo.getQuantidade() :
                atual.getQuantidade() < proximo.getQuantidade();
                
                if( deveTrocar ){
                    Item itemTrocado = atual;
                    this.itens.set(j, proximo);
                    this.itens.set(j + 1, itemTrocado);
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    












/*public class Inventario {
    
    //private Item [] itens;
    private ArrayList<Item> itens;
    
    /*public Inventario(){
        this.itens = new Item[99];
    }*/
    
    /*public Inventario(int quantidade){
        this.itens = new ArrayList<Item>(quantidade);
    }
    
    public ArrayList<Item> getItens(){
        return this.itens;
    }
        
    public Item obter(int posicao)
    {
        if(posicao >= this.itens.size()){
            return null;
        }
        
        return this.itens.get(posicao);
    }
    
    public void remover(Item item){
        //this.itens[posicao] = null;
        this.itens.remove(item);
    }
    
    public void adicionar(Item item){
        this.itens.add(item);
        /*for (int i = 0; i < this.itens.length; i++) {
            if (this.itens[i] == null) {
                this.itens[i] = item;
            }else{
                System.out.println("Inventário esta cheio!");
            }
        }
    }*/
    
    
    /*public String getDescricoesItens(){
        /*String lista = "";
        
        for(int i = 0; i < this.itens.size(); i++){
           lista += this.itens.get(i).getDescricao()+", ";
        }
        return lista;*/
         /* StringBuilder descricoes = new StringBuilder();
        for ( int i = 0; i < this.itens.size(); i++ ){
            Item item = this.itens.get(i);
            if ( item != null){
                descricoes.append(item.getDescricao());
                descricoes.append(",");
            
             }
            }
            
        return (descricoes.length() > 0 ?
        descricoes.substring(0, (descricoes.length() - 1)) : descricoes.toString());    
     }
            
    public Item getItemMaiorQuantidade(){
        //Item itemTemp = this.itens.get(0);
        
        /*for(int i = 0; i < this.itens.size(); i++)
        {
            if(itemTemp.getQuantidade() < this.itens.get(i).getQuantidade()){
                itemTemp = this.itens.get(i);
            }  
        }*/
        
        /*int indice = 0, maiorQuantidade = 0;
        
        for ( int i = 0; i < this.itens.size(); i++ ){
            Item item = this.itens.get(i);
            if ( item != null){
                if ( item.getQuantidade() > maiorQuantidade ) {
                    maiorQuantidade = item.getQuantidade();
                    indice = i;
                }
            }
            //return itemTemp;
            return this.itens.size() > 0 ? this.itens.get(indice) : null;
         }
    }
        
    public Item buscar( String descricao ){
        for (Item itemAtual : this.itens){
          boolean encontrei = itemAtual.getDescricao().equals(descricao);
           if ( encontrei ){
              return itemAtual;
          }
           return itemAtual;
          }
         
        /*for (int i = 0; i < this.itens.size(); i++)
        {
            if(this.itens.get(i).getDescricao().compareTo(descricao) == 0)
            {
                return this.itens.get(i);
            }
        }
        return null;
     }   */
    
   /* public ArrayList<Item> inverter(){
        ArrayList<Item> listaInvertida = new ArrayList<>(this.itens.size());
        for ( int i = this.itens.size() - 1; i >= 0; i--){
             listaInvertida.add(this.itens.get(i));
        }
        return listaInvertida;
        
        
        
        /*ArrayList<Item> arrayInverso = new ArrayList<Item>();
        
        for(int i = this.itens.size()-1 ; i >= 0 ; i--)
        {
            arrayInverso.add(this.itens.get(i));
        }
        
        return arrayInverso;*/
        
        /*ArrayList<Item> listaInvertida = new ArrayList<>(this.itens.size());
         * for (int i = this.itens.size()-1 ; i >= 0 ; i--){
         *   listaInvertida.add(this.itens.get(i));
        }
            return listaInvertida;
        
    }*/
    
    
    /*public void ordenarItens(){
        this.ordenarItens(TipoOrdenacao.ASC);
    }
    
    
    public void ordenarItens(TipoOrdenacao ordenacao){
        for (int i = 0;i < this.itens.size();i++){
            for (int j = 0; j < this.itens.size() - 1; j++){
                Item atual = this.itens.get(j);
                Item proximo = this.itens.get(j + 1);
                
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC?
                atual.getQuantidade() > proximo.getQuantidade():
                atual.getQuantidade() < proximo.getQuantidade();
                
                if(deveTrocar){
                    Item itemTrocado = atual;
                    this.itens.set(j, proximo);
                    this.itens.set(j + 1, itemTrocado);
                }
            }
        }
    
    }
    
    /*public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        for ( int i = 0; i < this.itens.length; i++){
           Item item = this.itens[i];
           if ( item != null ){
            descricoes.append (item.getDescricao());
            descricoes.append(" ,");
            }
        }
        return (descricoes.length () > 0 ?
        descricoes.substring(0, (descricoes.length() -1)):
        descricoes.toString()
        );
        
    }*/
    
    /*public Item gerItemComMaiorQuantidade(){
        int indice = 0, maiorQuantidade = 0;
        
        for (int i = 0; i < this.itens.length; i++){
            Item item = this.itens [i];
            if (item.getQuantidade() > maiorQuantidade){
            maiorQuantidade = item.getQuantidade();
            indice = i;
            }
        }
        
        return this.itens.length > 0 ? this.itens[indice] : null;
    }*/
   
            
    
}
